﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour {

    [SerializeField] Vector3 following;

    bool tooFar = true;
    Camera camera;
    // Use this for initialization
    void Start()
    {
        camera = gameObject.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Transform player1transform = GameObject.Find("Player1").transform;
        Transform player2transform = GameObject.Find("Player2").transform;
        following = (player1transform.position + player2transform.position) / 2;

        float hypotenuse = Mathf.Sqrt((player1transform.position.x - player2transform.position.x) * (player1transform.position.x - player2transform.position.x) + (player1transform.position.y - player2transform.position.y) * (player1transform.position.y - player2transform.position.y));


        camera.orthographicSize = hypotenuse/2 + 4;



        //add player too and calculate the middle point and follow that.
        Vector3 offset = gameObject.transform.position - following;
        gameObject.transform.position = new Vector3(gameObject.transform.position.x - offset.x, gameObject.transform.position.y - offset.y, -10);

    }
}
