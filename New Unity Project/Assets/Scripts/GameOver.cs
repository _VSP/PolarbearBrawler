﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameOver : MonoBehaviour {


    WinnerCount winnerCount;
	// Use this for initialization
	void Start () {
        winnerCount = GameObject.Find("ScoreBoard").GetComponent<WinnerCount>();
	}
	
	// Update is called once per frame
	void Update () {
		if(gameObject.transform.position.y <= -10)
        {
            if (gameObject.name == "Player1") winnerCount.AddWintoPlayer(2);
            else if (gameObject.name == "Player2") winnerCount.AddWintoPlayer(1);
           SceneManager.LoadScene(2);
        }
	}
}
