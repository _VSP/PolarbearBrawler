﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleManager : MonoBehaviour
{
	[SerializeField, Tooltip("Set panel to fadeout")]
	Image fade_panel_;

	[SerializeField, Tooltip("frame of fadein")]
	int fadein_frame_;

	[SerializeField, Tooltip("frame of fadeout")]
	int fadeout_frame_;

	int counter_ = 0;

	bool is_fade_out_;
	bool is_fade_in_;

	// Use this for initialization
	void Start()
	{
		fade_panel_ = GameObject.Find("FadePanel").GetComponent<Image>();

		StartFadeIn();
	}

	// Update is called once per frame
	void Update()
	{
		if (is_fade_in_)
		{
			FadeIn();
		}
		else if (is_fade_out_)
		{
			FadeOut();
		}
	}

	void FadeIn()
	{
		counter_++;

		if (counter_ > fadein_frame_)
		{
			Time.timeScale = 1.0f;
			//Destroy(gameObject);
		}

		fade_panel_.color = new Color(
			fade_panel_.color.r, fade_panel_.color.g, fade_panel_.color.b, 1.0f - (counter_ / (float)fadein_frame_));
	}

	void FadeOut()
	{
		counter_++;

		if (counter_ > fadeout_frame_)
		{
			Time.timeScale = 1.0f;
			//Destroy(gameObject);
		}

		fade_panel_.color = new Color(
			fade_panel_.color.r, fade_panel_.color.g, fade_panel_.color.b, counter_ / (float)fadeout_frame_);
	}

	public void StartFadeIn()
	{
		Time.timeScale = 0.0f;

		counter_ = 0;

		is_fade_in_ = true;

	}

	public void StartFadeOut()
	{
		Time.timeScale = 0.0f;

		counter_ = 0;
		is_fade_out_ = true;
	}
}
