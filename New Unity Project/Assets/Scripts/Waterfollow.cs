﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waterfollow : MonoBehaviour
{

    [SerializeField] Vector3 following;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        following = (GameObject.Find("Player1").transform.position + GameObject.Find("Player2").transform.position) / 2;

        //add player too and calculate the middle point and follow that.
        Vector3 offset = gameObject.transform.position - following;
        gameObject.transform.position = new Vector3(gameObject.transform.position.x - offset.x / 10, 4, 0);

    }
}