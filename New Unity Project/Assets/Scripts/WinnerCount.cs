﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinnerCount : MonoBehaviour {


    int player1Wins;
    int player2Wins;

    Text Player1Wins;
    Text Player2Wins;

	// Use this for initialization
	void Start () {
        player1Wins = 0;
        player2Wins = 0;

	}

    public void AddWintoPlayer(int player)
    {

        Player1Wins = GameObject.Find("Player1Wins").GetComponent<Text>();
        Player2Wins = GameObject.Find("Player2Wins").GetComponent<Text>();


        if (player == 1)
        {
            player1Wins++;
            Player1Wins.text = "Player1 Wins: " + player1Wins;
        }
        else if (player == 2)
        {
            player2Wins++;
            Player2Wins.text = "Player2 Wins: " + player2Wins;
        }
    }
	
	// Update is called once per frame
	void Update () {
        Player1Wins = GameObject.Find("Player1Wins").GetComponent<Text>();
        Player2Wins = GameObject.Find("Player2Wins").GetComponent<Text>();

        if (Player1Wins != null)
        {
            Player1Wins.text = "Player1 Wins: " + player1Wins;
        }

        if(Player2Wins != null)
        {
            Player2Wins.text = "Player2 Wins: " + player2Wins;
        }
	}
}
