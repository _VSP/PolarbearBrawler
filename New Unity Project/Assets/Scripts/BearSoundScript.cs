﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearSoundScript : MonoBehaviour {

    [SerializeField]List<AudioClip> audioClips = new List<AudioClip>();
    AudioSource audioSource;

    private void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    public void PlayThrowBall()
    {
        audioSource.PlayOneShot(audioClips[0]);
    }

    public void PlayBearSpawn()
    {
        Debug.Log("BearSpawn");
        audioSource.PlayOneShot(audioClips[1]);
    }

    public void PlayBearJump()
    {
        audioSource.PlayOneShot(audioClips[2]);
    }

    public void PlayBearDeath()
    {
        audioSource.PlayOneShot(audioClips[3]);
    }


}
