﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayTogether : MonoBehaviour {

    [SerializeField]List<GameObject> myFriends = new List<GameObject>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		foreach(GameObject icePixel in myFriends)
        {
            Vector2 direction = gameObject.transform.position - icePixel.transform.position;
            icePixel.GetComponent<Rigidbody2D>().AddForce(direction * 10);
        }
	}

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "IcePixel")
        {
            myFriends.Add(collider.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "IcePixel")
        {
            myFriends.Remove(collider.gameObject);
        }
    }
}
