﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateIceBerg : MonoBehaviour {


    [SerializeField] GameObject IcePixel;
    [SerializeField] List<Sprite> spriteList = new List<Sprite>();
    int a = 0;
    // Use this for initialization
	void Start () {
       
        
	}
	
	// Update is called once per frame
	void Update () {
        a++;
        if(a<= 30)
        {
            Vector2 randomForce = new Vector2(Random.Range(-100, 100), Random.Range(-100, 100));
            GameObject newPixel = Instantiate(IcePixel);
            newPixel.name = "IcePixel" + a;
            newPixel.transform.SetParent(GameObject.Find("IceBerg").transform);
            newPixel.GetComponent<SpriteRenderer>().sprite = spriteList[Random.Range(0, 5)];
            newPixel.GetComponent<Rigidbody2D>().AddForce(randomForce);
        }
        
    }
}
