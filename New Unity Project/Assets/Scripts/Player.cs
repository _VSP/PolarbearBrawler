﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
	PlayerAnimatorScript playerAnimatorScript;
	BearSoundScript bearSoundScript;

	enum PlayerNumber
	{
		Player1,
		Player2,
	}
	[SerializeField, Tooltip("Set player1 or 2")]
	PlayerNumber player_number_;

	[SerializeField, Tooltip("")]
	float walk_acc_ratio_;

	[SerializeField, Tooltip("Throw Power")]
	float throw_power_;

	[SerializeField, Tooltip("Throw Cooltime")]
	float cool_time_;

	float cooltime_counter_ = 0.0f;

	[SerializeField, Tooltip("Holding space delta changeing angle")]
	float angle_changing_speed_ = 2.0f;

	float min_angle_ = 10.0f;    //degree
	float max_angle_ = 80.0f;

	float current_angle_;

	[SerializeField, Tooltip("Jump Power")]
	float jump_power_;

	[SerializeField, Tooltip("Vacume Item radius. if no necessaly vacuming set value 0.0f ")]
	float vacume_radius_;

	[SerializeField, Tooltip("Item collider radius. it is not use collider2d")]
	float collide_radius_;

	[SerializeField, Tooltip("Set snowball asset")]
	GameObject snowball_;

	[SerializeField]
	float item_vacuming_power_;

	[SerializeField, Tooltip("time of max item effect")]
	//すでにアイテムの効果があった場合、この時間を加算して効果カウントも加算する？
	float max_item_time_;
	public float max_item_time { get { return max_item_time_; } }

	float left_big_snowball_time_ = 0.0f;
	public float left_big_snowball_time
	{
		get
		{
			return left_big_snowball_time_;
		}
	}

	float left_more_snowball_time_ = 0.0f;
	public float left_more_snowball_time
	{
		get
		{
			return left_more_snowball_time_;
		}
	}


	private int more_snowball_level_;   //item effect level. 1 is default level
	public int more_snowball_level
	{ get { return more_snowball_level_; } }

	public int big_snowball_level_;
	public int big_snowball_level { get { return big_snowball_level_; } }

	[SerializeField]
	int max_item_level_;

	[SerializeField, Tooltip("snowball is expanded pow(value,big_snowball_level_")]
	float snowball_expand_ratio_;

	[SerializeField, Tooltip("delta angle when add snowball ")]
	float add_snowball_delta_;  //2個目以降の雪玉につける角度

	bool is_jumping_;   //Ban to jump infinity

	private Rigidbody2D rigidbody_;

	private LineRenderer line_renderer_;

	Slider big_slider_;
	Text big_text_;
	Slider more_slider_;
	Text more_text_;

	string jump_button_;
	string move_axis_;
	string throw_button_;

	// Use this for initialization
	void Start()
	{
		rigidbody_ = GetComponent<Rigidbody2D>();

		current_angle_ = min_angle_;

		line_renderer_ = GetComponent<LineRenderer>();

		Debug.Assert(snowball_, "DON'T SET SNOWBALL");

		bearSoundScript = GameObject.Find("GameManager").GetComponent<BearSoundScript>();

		switch (player_number_)
		{
			case PlayerNumber.Player1:
				playerAnimatorScript = GameObject.Find("Player1").GetComponent<PlayerAnimatorScript>();
				jump_button_ = "Jump1";
				move_axis_ = "Horizontal1";
				throw_button_ = "ThrowSnowball1";
				break;

			case PlayerNumber.Player2:
				playerAnimatorScript = GameObject.Find("Player2").GetComponent<PlayerAnimatorScript>();
				jump_button_ = "Jump2";
				move_axis_ = "Horizontal2";
				throw_button_ = "ThrowSnowball2";

				break;
		}

		bearSoundScript.PlayBearSpawn();
	}

	// Update is called once per frame
	void Update()
	{
		Walk();

		Jump();

		ThrowSnowball();

		VacumeItems();

		UpdateItemEffect();

		//test code
		if (Input.GetKeyDown(KeyCode.I))
		{
			ActivateMoreSnowball();
		}
		else if (Input.GetKeyDown(KeyCode.O))
		{
			ActivateBigSnowball();
		}
	}

	void Jump()
	{
		if (!is_jumping_)
		{
			if (Input.GetButton(jump_button_))
			{

				rigidbody_.AddForce(Vector2.up * jump_power_);
				is_jumping_ = true;
			}
		}
	}

	void ThrowSnowball()
	{

		//changing angle
		if (Input.GetButton(throw_button_))
		{

			current_angle_ += angle_changing_speed_;
			if (current_angle_ > max_angle_)
			{
				current_angle_ = max_angle_;
				angle_changing_speed_ *= -1.0f;
			}
			else if (current_angle_ < min_angle_)
			{
				current_angle_ = min_angle_;
				angle_changing_speed_ *= -1.0f;
			}

			line_renderer_.enabled = true;
		}
		else
		{
			line_renderer_.enabled = false;
		}

		//Update cooltime
		cooltime_counter_ -= Time.deltaTime;
		if (cooltime_counter_ > 0.0f)
		{
			line_renderer_.enabled = false;

			return;     //cant shot
		}
		else
		{
			cooltime_counter_ = 0.0f;
		}

		//throw ball
		List<Vector3> throw_dirs = new List<Vector3>(more_snowball_level_);

		//右方向との内積で、プレイヤーが右向いてるのか左向いてるのか判定する
		bool is_right = Vector2.Dot(transform.right, Vector2.right) > 0.0f;

		//1投目の方向
		if (is_right)
		{
			throw_dirs.Add(Quaternion.AngleAxis(current_angle_, Vector3.forward) * transform.right);
		}
		else
		{
			throw_dirs.Add(Quaternion.AngleAxis(-current_angle_, Vector3.forward) * transform.right);
		}

		if (Input.GetButtonUp(throw_button_))
		{
			playerAnimatorScript.TriggerAttackAnimation();

			bearSoundScript.PlayThrowBall();

			List<GameObject> clone_snowballs = new List<GameObject>();
			for (int i = 0; i < more_snowball_level_; i++)
			{
				//increase snowball by item effect
				clone_snowballs.Add(Instantiate<GameObject>(snowball_));
			}

			//expand scale by item effect
			{
				float expand_ratio = (float)Math.Pow(snowball_expand_ratio_, (float)(big_snowball_level_ - 1));
				foreach (var clone_snowball in clone_snowballs)
				{
					clone_snowball.transform.localScale = new Vector3(expand_ratio, expand_ratio, expand_ratio);
				}
			}

			for (int i = 1; i < more_snowball_level_; i++)
			{
				if (i % 2 == 0)
				{
					float delta = (i + 1) / 2 * add_snowball_delta_;

					//偶数番目であれば下方向に増やす
					if (is_right)
					{
						throw_dirs.Add(Quaternion.AngleAxis(current_angle_ - delta, Vector3.forward) * transform.right);
					}
					else
					{
						throw_dirs.Add(Quaternion.AngleAxis(-current_angle_ + delta, Vector3.forward) * transform.right);
					}
				}
				else
				{
					//奇数番目であれば上方向に増やす
					float delta = (i + 1) / 2 * add_snowball_delta_;

					if (is_right)
					{
						throw_dirs.Add(Quaternion.AngleAxis(current_angle_ + delta, Vector3.forward) * transform.right);
					}
					else
					{
						throw_dirs.Add(Quaternion.AngleAxis(-current_angle_ - delta, Vector3.forward) * transform.right);
					}

				}
			}


			//setting parameter
			for (int i = 0; i < clone_snowballs.Count; i++)
			{
				clone_snowballs[i].transform.position = transform.position + throw_dirs[i];
				clone_snowballs[i].GetComponent<Rigidbody2D>().AddForce(throw_dirs[i] * throw_power_);
			}

			cooltime_counter_ = cool_time_;
		}

		//draw trajection line
		if (line_renderer_.enabled)
		{
			line_renderer_.SetPosition(0, transform.position);
			line_renderer_.SetPosition(1, transform.position + throw_dirs[0] * 10.0f);
		}

	}

	void Walk()
	{
		float hor_input = Input.GetAxis(move_axis_);  //XINPUT or WASD

		if (hor_input < 0)
		{
			transform.rotation = Quaternion.AngleAxis(180, Vector3.up);

		}
		else if (hor_input > 0)
		{
			transform.rotation = Quaternion.identity;

		}
		playerAnimatorScript.TriggerMovingAnimation();
		rigidbody_.AddForce(Vector2.right * hor_input * walk_acc_ratio_);
	}

	void VacumeItems()
	{
		var items = GameObject.FindGameObjectsWithTag("Items");
		foreach (var item in items)
		{
			var dir = transform.position - item.transform.position;
			if (dir.magnitude < vacume_radius_)
			{
				dir.Normalize();
				item.transform.position += dir * item_vacuming_power_ * Time.deltaTime;
			}

			//Collide Item
			if (Vector2.Distance(transform.position, item.transform.position) < collide_radius_)
			{
				//TODO:Judge Item
				//TODO:Effect
				var kind_item = item.GetComponent<ItemEffect>().type_items;
				switch (kind_item)
				{
					case ItemEffect.TypeItems.kBigSnowball:
						ActivateBigSnowball();
						break;

					case ItemEffect.TypeItems.kMoreSnowball:
						ActivateMoreSnowball();
						break;

				}
				Destroy(item);
			}

		}

	}

	void UpdateItemEffect()
	{
		left_more_snowball_time_ -= Time.deltaTime;
		left_big_snowball_time_ -= Time.deltaTime;
		Debug.Log(left_big_snowball_time_);
		if (left_more_snowball_time_ < 0.0f)
		{
			more_snowball_level_ = 1;
			left_more_snowball_time_ = 0.0f;
		}

		if (left_big_snowball_time_ < 0.0f)
		{
			big_snowball_level_ = 1;
			left_big_snowball_time_ = 0.0f;
		}
	}

	void ActivateMoreSnowball()
	{
		left_more_snowball_time_ = max_item_time_;
		more_snowball_level_++;
		if (more_snowball_level_ > max_item_level_)
		{
			more_snowball_level_ = max_item_level_;
		}
	}

	void ActivateBigSnowball()
	{
		left_big_snowball_time_ = max_item_time_;
		big_snowball_level_++;
		if (big_snowball_level_ > max_item_level_)
		{
			big_snowball_level_ = max_item_level_;
		}
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "IcePixel") //is it right?
		{
			is_jumping_ = false;
		}
	}

}
