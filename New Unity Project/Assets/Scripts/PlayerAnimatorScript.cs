﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimatorScript : MonoBehaviour {


    Animator animator;
	// Use this for initialization
	void Start () {
        animator = gameObject.GetComponent<Animator>();
        
        
    }

    public void TriggerAttackAnimation()
    {
        animator.SetTrigger("PlayerAttack");
    }

    public void TriggerMovingAnimation()
    {
        animator.SetTrigger("PlayerMoving");
    }

    public void TriggerJumpAnimation()
    {
        animator.SetTrigger("PlayerJump");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
