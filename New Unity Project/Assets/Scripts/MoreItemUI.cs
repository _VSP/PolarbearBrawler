﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoreItemUI : MonoBehaviour {

	[SerializeField, Tooltip("set player asset")]
	Player player_script_;

	Slider slider;
	Text text;

	// Use this for initialization
	void Start()
	{
		Debug.Assert(player_script_, "Dont Set Player Asset");

		slider = GetComponentInChildren<Slider>();
		slider.maxValue = player_script_.max_item_time;

		text = GetComponentInChildren<Text>();
	}

	// Update is called once per frame
	void Update()
	{
		text.text = "More	Lv." + player_script_.more_snowball_level;

		slider.value = player_script_.left_more_snowball_time;
	}
}
