﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillMe : MonoBehaviour {

	// Use this for initialization
	void Start () {

        StartCoroutine(Example());
    }

    IEnumerator Example()
    {
        print(Time.time);
        yield return new WaitForSeconds(0.3f);
        Destroy(gameObject);
        print(Time.time);
    }
}

