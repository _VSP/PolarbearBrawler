﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashScript : MonoBehaviour {


    [SerializeField] GameObject Splosh;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            Instantiate(Splosh);
            Splosh.transform.position = collider.gameObject.transform.position;
        }
    }
}
