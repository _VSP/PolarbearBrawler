﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//I dont know the rect of spawning
//so I temporary use camera rect and camera pos
public class ItemSpawner : MonoBehaviour
{
	Camera main_camera;

	Vector2 left_up_;
	Vector2 right_up_;

	[SerializeField, Tooltip("when elapsed this time, spawn item")]
	float spawn_period_;    //

	[SerializeField, Tooltip("spawn_period +- this time")]
	float random_time_width_;

	float timer_ = 0.0f;
	float reserved_time_;

	[SerializeField, Tooltip("Set Item prefab")]
	GameObject[] items_;

	// Use this for initialization
	void Start()
	{
		main_camera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
		Debug.Assert(main_camera, "Set Maincamera");

		reserved_time_ = spawn_period_;
	}

	// Update is called once per frame
	void Update()
	{
		timer_ += Time.deltaTime;

		if (timer_ > reserved_time_)
		{
			GetCameraRect();

			SpawnItem();
			
		}
	}

	void GetCameraRect()
	{
		left_up_ = main_camera.ScreenToWorldPoint(Vector3.zero);
		left_up_.Scale(new Vector3(1.0f, -1.0f, 1.0f));

		right_up_ = main_camera.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, 0.0f));
		right_up_.Scale(new Vector3(1.0f, -1.0f, 1.0f));
	}

	void SpawnItem()
	{
		reserved_time_ = spawn_period_ + Random.Range(-random_time_width_, random_time_width_);
		timer_ = 0.0f;

		Vector3 spawn_pos;
		spawn_pos = new Vector3(
			Random.Range(left_up_.x, right_up_.x),
			transform.position.y, 0.0f);

		int spawn_item = Random.Range(0, items_.Length);
		var obj=Instantiate (items_[spawn_item]);
		obj.transform.position = spawn_pos;
	}
}