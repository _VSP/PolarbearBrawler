﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemEffect : MonoBehaviour
{
	public enum TypeItems
	{
		kMoreSnowball,
		kBigSnowball,

	}
	[SerializeField,Tooltip("set item type")]
	TypeItems type_items_;
	public TypeItems type_items
	{
		get
		{
			return type_items_;
		}
	}

	// Use this for initialization
	void Start()
	{
		
	}

	// Update is called once per frame
	void Update()
	{
		//DO NOTING
	}
}
